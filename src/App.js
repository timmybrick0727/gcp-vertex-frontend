import React, { useState } from "react";
import axios from "axios";

const SearchBox = () => {
  const [query, setQuery] = useState("");
  const [data, setData] = useState([]);

  const handleInputChange = (e) => {
    setQuery(e.target.value);
  };

  const handleSearch = async () => {
    const token =
      "ya29.a0AfB_byDPTbtr7XxG3OmedTjv7ms71uAMYQmvJl0Do7AtMO74McZjyYLX8aGcF0zz2UEU7sMtoV3KJ4T0h-9toTZnglLv-JqBQlLQ4l9ejbb4Xm5q8gamQjB5z1t_G6l_oHZlYpdip3wr1k6qYaf9ylh4RWgAs4ABOZH0mfxnBSMTkqvPbdTcJbpAleFAz7O3VoczwpX8VQ4D2_fk-GkT3HrN19EyabVoBzBV3KCbCfpVjaTkcqmBeBs2pPL_njzlmTZOxX3UA_GAl0-fsotIdenZAdCVGQKybcgXtlkij_jFDzCE1-taEA_rdK82BWvN_bkil23s2xEIeVUYBjL9qTBvuSHoSoKPzt9gLrwYhp6q0BE8PTzXAX9oNTLgczh8oxu3Q2tohnZQj-sXmUbM0kUjMCYw08TQ3gaCgYKAZoSARISFQGOcNnCvcGtQWp3jmwT5f5vjeMJrg0425";

    const headers = {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    };

    const data = {
      query: query,
      pageSize: 10,
      queryExpansionSpec: {
        condition: "AUTO",
      },
      spellCorrectionSpec: {
        mode: "AUTO",
      },
      contentSearchSpec: {
        snippetSpec: {
          returnSnippet: true,
        },
      },
    };

    try {
      const response = await axios.post(
        "https://discoveryengine.googleapis.com/v1alpha/projects/687867604416/locations/global/collections/default_collection/dataStores/demo-vertexai-datastore_1698026636810/servingConfigs/default_search:search",
        data,
        { headers: headers }
      );
      setData(response.data.results);
    } catch (error) {
      console.error("Error making the API call:", error);
    }
  };

  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          margin: "24px 0 24px 0",
        }}
      >
        <input
          type="text"
          value={query}
          onChange={handleInputChange}
          placeholder="输入搜索文本..."
        />
        <button onClick={handleSearch}>搜索</button>
      </div>
      <div
        style={{
          backgroundColor: "gray",
          width: "100%",
          height: "70%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "start",
          alignItems: "center",
          overflowY: "auto", // Add this to make it scrollable
        }}
      >
        {data.map((item, index) => {
          return (
            <div
              // key={index}
              style={{
                width: "70%",
                minHeight: "100px", // Here's the change
                backgroundColor: "white",
                margin: "12px 0 12px 0",
                borderRadius: "10px",
                boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.2)",
                display: "flex",
                flexDirection: "column",
                padding: "12px",
              }}
            >
              <div style={{ fontSize: "20px", fontWeight: "bold" }}>
                {item["document"]["derivedStructData"]["title"]}
              </div>
              <a
                href={item["document"]["derivedStructData"]["link"]}
                style={{ fontSize: "18px" }}
                target="_blank"
                rel="noopener noreferrer"
              >
                {
                  item["document"]["derivedStructData"]["snippets"][0][
                    "snippet"
                  ]
                }
              </a>
              {/* test */}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SearchBox;
